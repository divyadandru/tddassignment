package com.virtualpairprogrammers.isbntools;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Spy;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

public class ExampleTests {

    @Mock
    read file;
    SumOfDivisorsOfFactorialOfANumber n;
    SumOfDivisorsOfFactorialOfANumber n1;

    @Spy
    read file1;

    @Before
    public void setup() throws Exception
    {
        System.out.println("Started");
        file = mock(read.class);
        n=new SumOfDivisorsOfFactorialOfANumber();
        n.setreadobject(file);
    }

    @After
    public void setup1()
    {
        System.out.println("Successful");
    }




    @Test
    public void mainfunction() throws Exception
    {
        SumOfDivisorsOfFactorialOfANumber num=new SumOfDivisorsOfFactorialOfANumber();
        num.setreadobject(new read());
        System.out.println("Sum of Divisors of the factorial is "+num.sumOfDivisorsOfAFactorialOfANumber());
    }

    @Test
    public void changeonaccess() throws Exception{
        when(file.reader(anyString())).thenReturn(4);
        int sum=n.sumOfDivisorsOfAFactorialOfANumber();
        //verify(file).reader(anyString());
        System.out.println("Return Value of Verify "+ verify(file).reader(anyString()));
    }

    @Test
    public void validsum() throws Exception{
        when(file.reader(anyString())).thenReturn(4);
        int answer=n.sumOfDivisorsOfAFactorialOfANumber();
          //System.out.println(ans);
        assertEquals(60,answer);
    }

    @Test
    public void invalidsum() throws Exception{
        n1=new SumOfDivisorsOfFactorialOfANumber();
        file1=spy(new read());
        n1.setreadobject(file1);

        doReturn(4).when(file1).reader(anyString());
        int answer=SumOfDivisorsOfFactorialOfANumber.sumOfDivisorsOfAFactorialOfANumber();
        assertNotEquals(40,answer);
    }





}
